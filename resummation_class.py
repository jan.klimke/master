
"""
Created on Tue Apr  2 13:20:35 2024

@author: amber.klimke
"""

#import vegas
import matplotlib.pyplot as plt
import numpy as np
import sympy as sym
import scipy as sc
from scipy.special import gamma,digamma
from scipy.optimize import fsolve

class Resummation():
    
       
    def __init__(self,d, a, b, g, legs_out=['q','q'],LL_mode=False):
        self.G=[]
        self.H=[]
        self.resum=[]
        self.diff=[]
        self.final=legs_out
        self.switch=1
        if LL_mode:
            self.switch=0
        self.a=a
        self.b=b
        self.d=d
        self.g=g
        self.Ca=3
        self.TR=1/2
        self.alpha=0.1175
        self.Cf=4/3
        self.nf=5
        self.beta_0= (11*self.Ca-4*self.TR*self.nf)/(12*np.pi)
        self.R=[]
        self.K = self.Ca*(67/18-np.pi**2/6)-5/9*self.nf
        self.beta_1 = (17*self.Ca**2-5*self.Ca*self.nf-3*self.Cf*self.nf)/(24*np.pi**2)
    
    
    def T(self,L):
        lamb=self.alpha*self.beta_0*L
        return -1/(np.pi*self.beta_0)*np.log(1-2*lamb)

    #color-sensitive factor
    def B_l(self,o):
        if o=='q':
            return -3/4  
        if o=='g' :
            return -(11*self.Ca-4*self.TR*self.nf)/(12*self.Ca)
        
        print("Bro,wtf")
        return 0

    #self.Calculating radiator for leg l 
    def radiator1(self,L, a_l, b_l):
        lamb=self.beta_0*self.alpha*L
        #split in two parts r1+r2
        
        if b_l==0:
            r1 = -1/(2*np.pi*self.beta_0**2*self.alpha)*( 
                2*lamb/a_l+np.log(1-2*lamb/a_l)
            )
            
            r2=self.K/(4*np.pi**2*self.beta_0**2)*(
                np.log(1-2*lamb/a_l)+ 2/a_l*(lamb/(1-2/a_l*lamb)))  -self.beta_1/(2*np.pi*self.beta_0**3)*(                    
                    1/2*np.log(1-2*lamb/a_l)**2  +  (np.log(1-2*lamb/a_l)+2/a_l*lamb)/(1-2*lamb/a_l)
                                        )
                
                
        else:    


            r1 = 1/(2*np.pi*self.beta_0**2*self.alpha*b_l)*(
                (a_l - 2*lamb)*np.log(1-2*lamb/a_l)
                - (a_l+b_l - 2*lamb)*np.log(1-2*lamb/(a_l+b_l))
        
            )
        
            r2 = 1/b_l*(
        
                self.K/(4*np.pi**2*self.beta_0**2)*((a_l+b_l)*np.log(1 - 2*lamb /(a_l+b_l)) - a_l*np.log(1 - 2*lamb/a_l))
                + self.beta_1/(2*np.pi*self.beta_0**3)*(
                    a_l/2 * np.log(1-2*lamb/a_l)**2 - (a_l+b_l) /
                    2*np.log(1-2*lamb/(a_l+b_l))**2
                    + a_l*np.log(1-2*lamb/a_l) - (a_l+b_l)*np.log(1-2*lamb/(a_l+b_l))
                )
            )
        
        return (r1+self.switch*r2)

    #self.Calculate r' from T function
    def radiator2(self,L, a_l, b_l):
       
        lamb=L*self.alpha*self.beta_0
        if b_l==0:
            return 2/a_l**2*1/(np.pi*self.beta_0)*lamb/(1-2*lamb/a_l)
        else:
            return 1/b_l*(self.T(L/a_l)-self.T(L/(a_l+b_l)))
        
        
        
    
    # actual F self.Calculation for event-shape-observables 
    def additive_F(self,L):

        Cf = self.Colorfactor(self.final)
        R=0
        for l in range(len(Cf)):
            R+=Cf[l]*self.radiator2(L,self.a[l],self.b[l])
        
        result=np.exp(-R*np.euler_gamma)/gamma(1+R)
        return result
    
    def Colorfactor(self,o):
        if type(o)is list:
            res=[0,0]
            for i in range(len(o)):
                
                if o[i]=='q':
                    res[i]= 4/3  
                if o[i]=='g':
                    res[i]= 3
                    
            return res
        
        
        if o=='q':
            return 4/3  
        if o=='g' :
            return 3
        print(o)
        print("My sibling in Christ, this is QCD, choose either gluon or quarks, what are you doing.")
        return 
    
    #ignore for leptonic initial state
    def q(self,x, Q):
        g=0.3
        q_ = (1+self.alpha*self.beta_0*np.log(Q/self.alpha))**(g/(np.pi*2*self.beta_0))
        return 1
    
    #bringing the terms together
    def summation(self,v,d, a, b, g, final=['q','q']):
        L = -np.log(v)
        res=0
        m_f = self.alpha

        E = [1/2, 1/2]
        x = E
        Cf = self.Colorfactor(final)
        Q = 1
        Q_12 = 1
        lamb = self.alpha*self.beta_0*L
        if g != [1, 1]:
            # integral teil
            dbar = d
        else:
            dbar = d
    
        legs = 2
        
        ###self.Calculate the log resummed cumulative disself.TRibution
        for l in range(legs):
            res += -Cf[l]*(
                self.radiator1(L, a[l], b[l])+self.radiator2(L, a[l], b[l])*(np.log(dbar[l])-b[l]*np.log(2*E[l]/Q))
            +self.switch*self.B_l(final[l])*self.T(L/(a[l]+b[l]))
    
            )
            + np.log(self.q(x[l], np.exp(-2*L/(a[l]+b[l])*m_f**2)) / self.q(x[l], m_f**2))
    
        res += np.log( self.additive_F(L) ) 
        
        res[np.isnan(res)]=-1000
        res[np.isinf(res)]=-1000
        res[np.isinf(-res)]=-1000
        return res 
    
        
        
    def FO_summation(self,v,d,a,b,g,legs,N=1):
        L = -np.log(v)
        res=0
        m_f = self.alpha
        E = [1/2, 1/2]
        x = E
        Cf = self.Colorfactor(self.final)
        Q = 1
        Q_12 = 1
        
        lamb = self.alpha*self.beta_0*L
        
        if self.g != [1, 1]:
            # integral teil
            dbar = self.d
        else:
            dbar = self.d
    
        if N==0:
            return np.zeros(len(L))
        ###self.Calculate the log resummed cumulative disself.TRibution
        for l in range(len(legs)):
            res += -Cf[l]*(
                self.FO_radiator1(L, self.a[l], self.b[l],N=N)+self.FO_radiator2(L, self.a[l], self.b[l],N)*(np.log(dbar[l])-b[l]*np.log(2*E[l]/Q))
            +self.switch*self.B_l(self.final[l])*self.FO_T(L/(self.a[l]+self.b[l]),N=N)
    
            )
            #+ np.log(self.q(x[l], np.exp(-2*L/(a[l]+b[l])*m_f**2)) / self.q(x[l], m_f**2))

       
        return res
        
    
    def FO_radiator1(self,L, a_l, b_l,N=1):
        self.alpha=0.1175/(2*np.pi)
    
    
        Cf = self.Colorfactor(self.final)
        rad1,rad2=0,0
        if b_l!=0:
            
            for i in range(1,N+1):  
                
      
               
                rad1+=4*(4*np.pi*self.beta_0)**(i-1)*self.alpha**i*L**(i)*1/(i*(i+1)*b_l)*(    1/a_l**i-1/(a_l+b_l)**i  )
            
     
            for i in range(2,N+1):
                 
                 rad2+=self.K*4*(4*np.pi*self.beta_0)**(i-2)*self.alpha**i*L**(i)/(i*b_l)*(
                     
                     1/a_l**(i-1)-1/(a_l+b_l)**(i-1)
                     
                     )
            for i in range(3,N+1):
                 
                 rad2+=32*np.pi**2*self.beta_1*4*(4*np.pi*self.beta_0)**(i-3)*self.alpha**i*L**(i)/(i*b_l)*(np.euler_gamma+digamma(i) -1)*(
                     
                     1/a_l**(i-1)-1/(a_l+b_l)**(i-1)
                     
                     )
        else:
            for i in range(1,N+1):
                rad1+=4*(4*np.pi*self.beta_0)**(i-1)*self.alpha**i*L**(i)/((i+1)*a_l**(i+1))
         
            for i in range(2,N+1):
                     
                rad2+=self.K*4*(4*np.pi*self.beta_0)**(i-2)*self.alpha**i*L**(i)*(i-1)/(i*a_l**i)
                     
            for i in range(3,N+1):
                     
                rad2+=32*np.pi**2*self.beta_1*4*(4*np.pi*self.beta_0)**(i-3)*self.alpha**i*L**(i)*(i-1)/(i*a_l**i)*(np.euler_gamma+digamma(i) -1)
                
        self.alpha = 0.1175 
            
        return L*rad1+self.switch*rad2
    
    
    def FO_radiator2(self,L, a_l, b_l,N=1):
        self.alpha=0.1175/(2*np.pi)
        rad=0
        if len(L)!=0 and N==0:
            rad=np.zeros(len(L))
            
        if b_l!=0:
            for i in range(1,N+1):
                rad+=4*(4*np.pi*self.beta_0)**(i-1)*self.alpha**i*L**(i)/(i*b_l)*(
                    
                    1/a_l**i-1/(a_l+b_l)**i
                    
                    )
        else:
            for i in range(1,N+1):
                rad+=4*(4*np.pi*self.beta_0)**(i-1)*self.alpha**i*L**(i)*(
                    
                    1/a_l**(i+1)
                    
                    )
            
        
        self.alpha = 0.1175   
        return rad
    
        
    
        
    def FO_T(self,L,N=1):

        T=0
        self.alpha=0.1175/(2*np.pi)
        
        for i in range(1,N+1):
            T+=4*(4*np.pi*self.beta_0)**(i-1)*self.alpha**i*L**(i)/(i)
        
        self.alpha = 0.1175    
        
        return T
    
    def FO_additive_F(self,L,a=[1,1],b=[1,1],legs=['q','q'],N=1):
        
        R=0
        res=0
        Cf=self.Colorfactor(self.final)
        if N>1:

            for n in range(1,N+1):
                R=0
                for l in range(len(legs)):                
                    R+=Cf[l]*(self.FO_radiator2( L, a[l], b[l],N=n))
                    
                for i in range(2,int(N/n)+1):
                    res+= -np.pi**2   *1/12*(R)**i
            
        return 1+res
        
        
        
    def run(self,v,deltaF=1,diff=True):
        A=len(v)
        self.resum=np.exp(self.summation(v, self.d, self.a, self.b, self.g, self.final))
        if A>1:
            if diff:
                self.diff=(self.resum[1:]-self.resum[:A-1])/(v[1:A]-v[:A-1])*deltaF
            else:
                self.diff=(self.resum[1:A]-self.resum[:A-1])*deltaF
        return self.resum
    
    def run_FO(self,v,N=1,deltaF=1,diff=True):
        A=len(v)
        L=-np.log(v)
        res=0
        
        def exponentialpart(self,v,N):
            result=1
            for j in range(1,N+1):
                for i in range(1,int(N/j)+1):
                    result+=(self.FO_summation(v, self.d, self.a, self.b, self.g, self.final,j))**i/np.math.factorial(i)
            return result
        
        
        for i in range(1,N+1):
            for j in range(1,N-i+2):
                res+=exponentialpart(self,v,N=i)*self.FO_additive_F(L,self.a,self.b,self.final,N=j)
        self.resum=res
        if A>1:
            if diff:
                self.diff=(self.resum[1:A]-self.resum[:A-1])/(v[1:A]-v[:A-1])*deltaF
            else:
                self.diff=(self.resum[1:A]-self.resum[:A-1])*deltaF
        return self.resum
        
        
        
        
        
    def plot(self,logmode=False,cumulative=False,N=2000):
        t=np.linspace(0,1,N)
        y=self.run(t)
        
        if cumulative:
            if logmode:
                plt.semilogy(t,self.resum,label="resum")            
            else:                
                plt.plot(t,self.resum,label="resum")
                    
            plt.xlabel("V")
            plt.ylabel(r"$\sigma$")
            plt.show()
            
        if logmode: 
            plt.semilogy(t,self.diff,label="resum") 
        else:
            plt.plot(t[1::],self.diff,label="resum")
            
        plt.xlabel("V")
        plt.ylabel(r"$\frac{\mathrm{d}\sigma}{\sigma\mathrm{d}V}$")
        plt.show()
        
    def compare_g_q(self,logmode=False,efficiency=True):
        N=2000
        t=np.linspace(0,1,N)
        
        self.final=['q','q']
        self.run(t)
        if logmode:
            plt.semilogy(t[1::],self.diff,label="qq")            
        else:                
            plt.plot(t[1::],self.diff,label="qq")
                
     
        plt.ylim(0,np.max(self.diff)+1)
        self.final=['g','g']
        self.run(t)
        
        if logmode:
            plt.semilogy(t[1::],self.diff,label="gg")            
        else:                
            plt.plot(t[1::],self.diff,label="gg")
            
        plt.xlabel("V")
        plt.ylabel(r"$\frac{\mathrm{d}\sigma}{\sigma\mathrm{d}(V)}$")
        plt.legend()

        plt.show()
        
        if efficiency:                
         
            def inv(self,t):
                
                def quark_x(self,t,x=0,a=self.a,b=self.b,d=self.d,g=self.g):
                    return  np.exp(self.summation(t,d,a,b,g,['q','q']))-x
                
                r=[]
                for i in t:
                    r.append(fsolve(lambda l: quark_x(self,l,x=i),0.2))
                return  r
            

          
            M=inv(self,t)
            eff=np.exp(self.summation(M,self.d,self.a,self.b,self.g,['g','g']))
            plt.plot(t,eff,label="Quark vs. Gluon")
            plt.xlabel("Quark Efficiency")
            plt.ylabel("Gluon Efficiency")
            plt.ylim(0,1)
            plt.xlim(0,1)
            plt.legend()
            plt.show()
            return eff
        
        
    def save(self,path="/net/theorie/home/amber.klimke/Documents/Masterarbeit/master/Results"):
        path+=f"/resum_a={self.a},b={self.b},g={self.g}d={self.d}{self.final[0]}{self.final[1]}.txt"
        f= open(path,'w')
        for i in self.resum:
            f.write(f'{i}')
            f.write('\n')
        f.close()    
    
    def Thrust_analytic(self,T):
        
        thrust = self.Cf*self.alpha/(np.pi*2) * (2*(3*T**2 - 3*T+2) / (T*(1-T))* np.log((2*T-1)/(1-T)) - (3*(3*T-2)*(2-T))/(1-T))

        return thrust
                      
        
    def FO_cheap_run(self,v,N=1,deltaF=1,diff=True):
        A=len(v)
        L=-np.log(v)
        res=0
        Cl=self.Colorfactor(self.final)
        
        for l in range(len(self.final)):
            res+=-2/self.a[l]*Cl[l]/(self.a[l]+self.b[l])
        self.alpha=0.1175/(2*np.pi)
        res=res*self.alpha*L**2+res*self.a[0]*self.B_l(self.final[0])*self.alpha*L*2 
        self.alpha=0.1175
        
        self.resum=1+res
        if A>1:
            if diff:
                self.diff=(self.resum[1:A]-self.resum[:A-1])/(v[1:A]-v[:A-1])*deltaF
            else:
                self.diff=(self.resum[1:A]-self.resum[:A-1])
        return self.resum
    
    
    def FO_radiator1_sym(self,alpha,L, a_l, b_l,N=1):
        alpha*=1/(2*np.pi)
    
        rad1,rad2=0,0
        if b_l!=0:
            
            for i in range(1,N+1):                
      
               
                rad1+=4*(4*np.pi*self.beta_0)**(i-1)*alpha**i*L**(i+1)*1/(i*(i+1)*b_l)*(    1/a_l**i-1/(a_l+b_l)**i  )
     
            for i in range(2,N+1):
                 
                 rad2+=self.K*4*(4*np.pi*self.beta_0)**(i-2)*alpha**i*L**(i)/(i*b_l)*(
                     
                     1/a_l**(i-1)-1/(a_l+b_l)**(i-1)
                     
                     )
            for i in range(3,N+1):
                 
                 rad2+=32*np.pi**2*self.beta_1*4*(4*np.pi*self.beta_0)**(i-3)*alpha**i*L**(i)/(i*b_l)*(np.euler_gamma+digamma(i) -1)*(
                     
                     1/a_l**(i-1)-1/(a_l+b_l)**(i-1)
                     
                     )
        else:
            for i in range(1,N+1):
                rad1+=4*(4*np.pi*self.beta_0)**(i-1)*alpha**i*L**(i+1)/((i+1)*a_l**(i+1))
         
            for i in range(2,N+1):
                     
                rad2+=self.K*4*(4*np.pi*self.beta_0)**(i-2)*alpha**i*L**(i)*(i-1)/(i*a_l**i)
                     
            for i in range(3,N+1):
                     
                rad2+=32*np.pi**2*self.beta_1*4*(4*np.pi*self.beta_0)**(i-3)*alpha**i*L**(i)*(i-1)/(i*a_l**i)*(np.euler_gamma+digamma(i) -1)
                
        alpha*=2*np.pi 
            
        return rad1+self.switch*rad2
    
    
    def FO_radiator2_sym(self,L,alpha, a_l, b_l,N=1):
        alpha*=1/(2*np.pi)
        rad=0
        if N==0:
            rad=np.zeros(len(L))
            
        if b_l!=0:
            for i in range(1,N+1):
                rad+=4*(4*np.pi*self.beta_0)**(i-1)*alpha**i*L**(i)/(i*b_l)*(
                    
                    1/a_l**i-1/(a_l+b_l)**i
                    
                    )
        else:
            for i in range(1,N+1):
                rad+=4*(4*np.pi*self.beta_0)**(i-1)*alpha**i*L**(i)*(
                    
                    1/a_l**(i+1)
                    
                    )           
        
        alpha*=np.pi*2   
        return rad
    
    
    def FO_T_sym(self,alpha,L,N=1):

        T=0
        alpha*=1/(2*np.pi)
        
        for i in range(1,N+1):
            T+=4*(4*np.pi*self.beta_0)**(i-1)*alpha**i*L**(i)/(i)
        
        alpha *= 2*np.pi      
        return T
    
    
    def FO_additive_F_sym(self,L,alpha,a=[1,1],b=[1,1],legs=['q','q'],N=1):
        R=alpha-alpha
        res=1+ alpha-alpha
        Cf=self.Colorfactor(self.final)
        
        if N>1:
            for l in range(len(legs)):                
                R+=Cf[l]*(self.FO_radiator2_sym( L,alpha, self.a[l], self.b[l],N=N))
                
            for n in range(2,N+1):        
                res+= -np.pi**2   *1/12*(R)**n    
        return res
    
    
    
    def FO_symbolic(self,L,alpha,N=1):
    
        m_f = alpha
        E = [1/2, 1/2]
        x = E
        Q = 1
        Q_12 = 1
                
        if self.g != [1, 1]:
            # integral teil
            dbar = self.d
        else:
            dbar = self.d
        
        
        res=0
        C=self.Colorfactor(self.final)
        for l in range(len(self.final)):
            res+=-C[l]*(self.FO_radiator1_sym(alpha, L, self.a[l], self.b[l],N=N)+self.FO_radiator2_sym(L,alpha, self.a[l], self.b[l],N)*(np.log(dbar[l])-self.b[l]*np.log(2*E[l]/Q))
                        +self.switch*self.B_l(self.final[l])*self.FO_T_sym(L/(self.a[l]+self.b[l]),alpha,N=N))
        return res
        
        
    def run_FO_symbolic(self,v,N=1,deltaF=1,diff=True):
        A=len(v)
        alpha=sym.symbols('alpha')  
        L=sym.symbols('L')  
        res=1
        x=self.FO_symbolic(L,alpha,N=N)
        x2=x+(self.FO_additive_F_sym(L,alpha,N=N)-1)
     
        
      
        
        for i in range(1,N+1):
            res+=(x)**i/np.math.factorial(i)
            
        G=np.zeros((N+1,N+2))
        self.H=G.copy()
        for n in range(N+1):
            for m in range(N+2):             
        
                    #G[n,m]=(2*np.pi)**n*sym.collect(sym.expand(x),alpha).coeff(alpha,n).coeff(L,m)
        
                    G[n,m]=(2*np.pi)**n*sym.collect(sym.collect(sym.expand(x),alpha).coeff(alpha,n),L).coeff(L,m)
                    self.H[n,m]=(2*np.pi)**n*sym.collect(sym.collect(sym.expand(x2),alpha).coeff(alpha,n),L).coeff(L,m)
        self.G=G          
 
        res*=self.FO_additive_F_sym(L,alpha,N=N)
        d=alpha-alpha
        self.R=[]
        
        for i in range(N+1):
            r=sym.collect(sym.expand(res),alpha).coeff(alpha,i)
            self.R.append(r*(2*np.pi)**i)
            d+=r*alpha**i

        g=np.array([d.evalf(subs={alpha:self.alpha, L:-np.log(i)}) for i in v])
        self.resum=g
        if A>1:
            if diff:
                self.diff=(self.resum[1:A]-self.resum[:A-1])/(v[1:A]-v[:A-1])*deltaF
            else:
                self.diff=(self.resum[1:A]-self.resum[:A-1])*deltaF
        return self.resum   
    
    def logRmatch(self,v,deltaF=1,diff=True,endpoint=1,LO=[],NLO=[]):
            ######only for NLL+LO matching
        def  FO(i,NLO=False):
            C=self.Colorfactor(self.final)
            res=0
            for l in [0,1]:
                res+=float(-C[l]*(self.radiator1(i,self.a[l],self.b[l]) + self.B_l(self.final[l])*self.T(i/(self.b[l]+self.a[l]))))

                
            res+= np.log( self.additive_F(i) )     
            res+= float(  self.alpha/(2*np.pi)*(self.R[1].evalf(subs={sym.symbols('L')  :i})-self.H[1,1]*i-self.H[1,2]*i**2))
            
            if NLO:
                res+= (self.alpha/(2*np.pi))**2*( self.R[2].evalf(subs={sym.symbols('L')  :i})  -1/2*self.R[1].evalf(subs={'L':i})**2  - self.H[2,2]*i**2  - self.H[2,3]*i**3)

            
            return res
              

        def endpointf(i,vmax,NLO=False):
                y=np.exp(-i)

                L_=np.log(1/y-1/vmax+1) 
                C=self.Colorfactor(self.final)
                res=0
                for l in [0,1]:
                    res+=float(-C[l]*(self.radiator1(i,self.a[l],self.b[l]) + self.B_l(self.final[l])*self.T(i/(self.b[l]+self.a[l]))))
                    
                res+= np.log( self.additive_F(L_) ) 
                    
                res+=float(self.alpha/(2*np.pi)*(self.R[1].evalf(subs={sym.symbols('L')  :i})-self.H[1,1]*L_-self.H[1,2]*L_**2) )

                if NLO:
                    res+= (self.alpha/(2*np.pi))**2*( self.R[2].evalf(subs={sym.symbols('L')  :i})  -1/2*self.R[1].evalf(subs={'L':i})**2  - self.H[2,2]*L_**2  - self.H[2,3]*L_**3)
                return res
                    

        def  FO_A(i,NLO=False):
            Cf=self.Colorfactor(self.final)
            y=np.exp(-i)
            res=0
           

 
            


            for l in range(len(self.final)):
                res += -Cf[l]*(
                    self.radiator1(i, self.a[l], self.b[l])            +self.B_l(self.final[l])*self.T(i/(self.a[l]+self.b[l]))    
                    )
    
            res += np.log( self.additive_F(i) )  

            res+= float(self.alpha/(2*np.pi)*(self.R[1](y) -self.H[1,1]*i-self.H[1,2]*i**2)) 

            if NLO:
                res += (self.alpha/(2*np.pi))**2*( self.R[2](y) -1/2*self.R[1](y)**2  - self.H[2,2]*i**2  - self.H[2,3]*i**3)
         #       print(np.array([self.R[2](y)- self.G[2,2]*i**2 - self.G[2,3]*i**3, self.R[1](y) -self.G[1,1]*i-self.G[1,2]*i**2  ]))
        
            
            return res
                       
        def endpointf_A(i,vmax,NLO=False):
                y=np.exp(-i)
                L_=np.log(1/y-1/vmax+1) 
                C=self.Colorfactor(self.final)
                res=0
                for l in [0,1]:
                    res+= float( -C[l]*(self.radiator1(L_,self.a[l],self.b[l]) + self.B_l(self.final[l])*self.T(L_/(self.b[l]+self.a[l])))) 

                res+= np.log( self.additive_F(L_) ) 
            
                res+= float(self.alpha/(2*np.pi)*(  self.R[1](y)   -self.H[1,1]*L_-self.H[1,2]*L_**2) ) 
            
                if NLO:
                    res+= (self.alpha/(2*np.pi))**2*( self.R[2](y)  -1/2*self.R[1](y)**2  - self.H[2,2]*L_**2  - self.H[2,3]*L_**3)
                return res


        
        L=-np.log(v)
        if NLO==[]:
            self.run_FO_symbolic(v,deltaF=deltaF,diff=diff,N=1)
        else:
            self.run_FO_symbolic(v,deltaF=deltaF,diff=diff,N=2)
        A=len(L)
        if LO!=[]:
            self.R[1]=LO
            if NLO==[]:
            
                
                if endpoint==1:
                    res=[FO_A(i) for i in L]
                else:
                    res=[endpointf_A(i,endpoint) for i in L ]
            else:
                self.R[2]=NLO

                if endpoint==1:
                    res=[FO_A(i,NLO=True) for i in L]
                else:
                    res=[endpointf_A(i,endpoint,NLO=True) for i in L ]
                
            ####B=NLO-LO-1 etc for NLO
            

        elif NLO==[]:
            if endpoint==1:
                res=[FO(i) for i in L]
            else:
                res=[endpointf(i,endpoint) for i in L and i <=endpoint]

                
        res=np.array(res)
        res[v>=endpoint]=np.nanmax(res[v<endpoint]) 
        self.resum=np.exp(res)

        if diff:
            self.diff=(self.resum[1:A]-self.resum[:A-1])/(v[1:A]-v[:A-1])*deltaF
        else:
            self.diff=(self.resum[1:A]-self.resum[:A-1])*deltaF

        return self.resum

    
    def helper_thrust(self,v):
        L=-np.log(v)
        res=self.alpha/(2*np.pi)*self.Cf*(4*L-3)*np.exp(L)
        return res
    
    def helper_thrust_integrated(self,v):
        L=-np.log(v)
        res=1-self.alpha/(2*np.pi)*self.Cf*(2*L**2-3*L)
        return res
    
    def get_LO(self, array,v,mult_alphaS=True):
        arr=list(array)
    
        arr.append(array[len(array)-1])
        v_=list(v)
        v_.append(1)
        arr=np.array(arr)
        f=1
        if mult_alphaS:
            f=2*np.pi/(self.alpha)
        else:
             f=1
        return sc.interpolate.interp1d(v_,f*arr)

    def get_NLO(self,LO,NLO,v):
            arr=list(NLO)
        
            arr.append(NLO[len(NLO)-1])
            LO_=list(LO)
            LO_.append(LO[len(LO)-1])
            LO_=np.array(LO_)
            v_=list(v)
            v_.append(1)
            arr=np.array(arr)
            arr+= -self.alpha/(2*np.pi)*LO_ -1
            res=4*np.pi**2*arr/(self.alpha)**2
            res*=1/(2*np.pi/self.alpha)**2
            return sc.interpolate.interp1d(v_,res)
        
    def update(self): 
        self.beta_0= (11*self.Ca-4*self.TR*self.nf)/(12*np.pi)
        self.K = self.Ca*(67/18-np.pi**2/6)-5/9*self.nf
        self.beta_1 = (17*self.Ca**2-5*self.Ca*self.nf-3*self.Cf*self.nf)/(24*np.pi**2)
        
        
        
            
            
                   
