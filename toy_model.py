import vegas
import matplotlib.pyplot as plt
import numpy as np
from scipy.special import gamma
# implementing NLL resummation procedure




#NLO prediction for thrust 
def Thrust_analytic(T):
    alpha = 0.1175
    thrust = 6*alpha/(np.pi*2) * (2*(3*T**2 - 3*T+2) / (T*(1-T))
                                  * np.log((2*T-1)/(1-T)) - (3*(3*T-2)*(2-T))/(1-T))

    return thrust

#cumulative NLO thrust
def Thrust_analytic_cum(T):
    N=len(T)
    res=[0]
    for i in range(1,N):
        if np.isnan(Thrust_analytic(T[i])) or np.isinf(Thrust_analytic(T[i])):
            res.append(0)
        else:
            res.append(1/N*Thrust_analytic(T[i])+res[i-1])
    return res


#defining T function
def T(L):
    Ca = 3
    TR = 1/2
    alpha=0.1175
    nf = 6
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb=alpha*beta_0*L
    return -1/(np.pi*beta_0)*np.log(1-2*lamb)

#color-sensitive factor
def B_l(o):
    Ca = 3
    TR = 1/2
    nf = 6
    if o=='q':
        return -3/4  
    if o=='g' :
        return -(11*Ca-4*TR*nf)/(12*Ca)
    
    print("Bro,wtf")
    return 0

#calculating radiator for leg l 
def radiator1(L, a_l, b_l):
    Ca = 3
    Cf=4/3
    alpha = 0.1175
    TR = 1/2
    nf = 6
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    beta_1 = (17*Ca**2)
    lamb = alpha*beta_0*L
    K = Ca*(67/18-np.pi**2/6)-5/9*nf
    
    #split in two parts r1+r2
    
    if b_l==0:
        r1 = -1/(2*np.pi*beta_0**2*alpha)*( 
            2*lamb/a_l+np.log(1-2*lamb/a_l)

        )
        r2=K**2/(4*np.pi**2*beta_0**2)*(
            np.log(1-2*lamb/a_l)+ 2/a_l*(lamb/(1-2/a_l*lamb))
            -beta_1/(2*np.pi*beta_0**3)*(
                
                1/2*np.log(1-2*lamb/a_l)**2  +  (np.log(1-2*lamb/a_l)+2/a_l*lamb)/(1-2*lamb/a_l)
                
                )
            
            )
    else:    


        r1 = 1/(2*np.pi*beta_0**2*alpha*b_l)*(
            (a_l - 2*lamb)*np.log(1-2*lamb/a_l)
            - (a_l+b_l - 2*lamb)*np.log(1-2*lamb/(a_l+b_l))
    
        )
    
        r2 = 1/b_l*(
    
            K/(4*np.pi**2*beta_0**2)*((a_l+b_l)*np.log(1 - 2*lamb /(a_l+b_l)) - a_l*np.log(1 - 2*lamb/a_l))
            + beta_1/(2*np.pi*beta_0**3)*(
                a_l/2 * np.log(1-2*lamb/a_l)**2 - (a_l+b_l) /
                2*np.log(1-2*lamb/(a_l+b_l))**2
                + a_l*np.log(1-2*lamb/a_l) - (a_l+b_l)*np.log(1-2*lamb/(a_l+b_l))
            )
        )
    
    return (r1+r2)

#calculate r' from T function
def radiator2(L, a_l, b_l):
    Ca = 3
    Cf=4/3
    alpha = 0.1175
    TR = 1/2
    nf = 6
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    beta_1 = (17*Ca**2)
    lamb=L*alpha*beta_0
    if b_l==0:
        return 2/a_l**2*1/(np.pi*beta_0)*lamb/(1-2*lamb/a_l)
    else:
        return 1/b_l*(T(L/a_l)-T(L/(a_l+b_l)))

    

#born-level color structure , for e+ e- -> q q trivial,   can be ignored for thrust
def Softfunction(L):
    Q=1
    Q_12 = 1
    Cf=4/3
    S = np.exp(-2*Cf*L*np.log(Q_12/Q))  # damn, soft function got hands
    return S

#2nd order approximation to F
def F_cheap(C, L):
    res = 1-np.pi**2/12*L**2
    return res


# actual F calculation for event-shape-observables 
def thrust_F(C,L,a=[1,1],b=[1,1],legs=['q','q']):
    Cf=4/3
    Ca = 3
    TR = 1/2
    nf = 6
    legs=len(a)
    alpha = 0.1175
    K = Ca*(67/18-np.pi**2/6)-5/9*nf
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb =alpha* beta_0*L
    beta_1 = (17*Ca**2)
    
    R=0
    for l in range(legs):
        R+=Cf*(T(L/a[l])-T(L/(a[l]+b[l]))) 
        
    #R=2*Cf*(   T(L/a[0])-T(L/(a[0]+b[0])) )  
        
    #2*Cf/(np.pi*beta_0**2)*(  np.log(1-2*lamb) - np.log(1-lamb)   )  
    ##
    
    result=np.exp(-R*np.euler_gamma)/gamma(1+R)
    return result

def Colorfactor(o):
    if type(o)is list:
        res=[0,0]
        for i in range(len(o)):
            
            if o[i]=='q':
                res[i]= 4/3  
            if o[i]=='g':
                res[i]= 3
                
        return res
    
    
    if o=='q':
        return 4/3  
    if o=='g' :
        return 3
    print(o)
    print("My sibling in Christ, this is QCD, choose either gluon or quarks, what are you doing.")
    return 

#ignore for leptonic initial state
def q(x, Q):
    g=0.3
    nf=6
    b0=(33-2*nf)/(12*np.pi)
    alpha =0.1175
    q_ = (1+alpha*b0*np.log(Q/alpha))**(g/(np.pi*2*b0))
    return 1

#bringing the terms together
def Resummation(v, d, a, b, g, final=['q','q']):
    L = -np.log(v)
    res = 0
    alpha = 0.1175
    m_f = alpha
    Ca = 3
    TR = 1/2
    nf = 6
    E = [1/2, 1/2]
    x = E
    Cf = Colorfactor(final)
    Q = 1
    Q_12 = 1
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb = alpha*beta_0*L
    if g != [1, 1]:
        # integral teil
        dbar = d
    else:
        dbar = d

    legs = 2
    
    ###calculate the log resummed cumulative distribution
    for l in range(legs):
        res += -Cf[l]*(
            radiator1(L, a[l], b[l])+radiator2(L, a[l], b[l])*(np.log(dbar[l])-b[l]*np.log(2*E[l]/Q))
        +B_l(final[l])*T(L/(a[l]+b[l]))

        )
        + np.log(q(x[l], np.exp(-2*L/(a[l]+b[l])*m_f**2)) / q(x[l], m_f**2))

    res += np.log(Softfunction(T(L/a[0]))) + np.log( thrust_F(Cf, L) ) 

    return res


def validate_Resummation(v, d, a, b, g, final=['q','q']):
    res=Resummation(v, d, a, b, g, final)
    N=len(v)
    diff=N*(res[1::]-res[:N-1:])
    order_alpha=-diff[1]/v
    return order_alpha




###compact resummation algorithm specificaly for e+ e- thrust
def thrust_resummation(v):
    L = -np.log(v)
    res = 0
    alpha = 0.1175
    m_f = alpha
    Ca = 3
    TR = 1/2
    nf = 6
    Cf = 4/3
    K = Ca*(67/18-np.pi**2/6)-5/9*nf
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb = alpha*beta_0*L
    beta_1 = (17*Ca**2)
    B=-3/2
    #lamb*=-1
    g1=-Cf/(np.pi*beta_0**2)*(
        
        (1-2*lamb)*np.log(1-2*lamb) - 2* (1-lamb)*np.log(1-lamb)
        
        )
    
    
    g2=(-Cf*K/(2*np.pi**2*beta_0**2)*(     
        2*np.log(1-lamb) -  np.log(1-2*lamb)        
        )  + Cf*B/(np.pi*beta_0)* ( np.log(1-lamb)) + Cf*beta_1/(np.pi*beta_0**3)* ( 
            2*np.log(1-lamb)-np.log(1-2*lamb)+np.log(1-lamb)**2 - 1/2* np.log(1-2*lamb)**2)
    -2*Cf*np.euler_gamma/(np.pi*beta_0)* (  np.log(1-lamb) -np.log(1-2*lamb)))
            
            
            
    
    g1_prime=-Cf/(np.pi*beta_0**2)*  (

        -2*np.log(1-2*lamb)+2*np.log(1-lamb)

    )
    
    
    return np.exp(1/alpha *g1+g2)/gamma(1-beta_0*g1_prime)
    
    
#### F function of thrust resum for comparison to thrust_F    
def thrust_F2(v):
    L = -np.log(v)
    res = 0
    alpha = 0.1175
    m_f = alpha
    Ca = 3
    TR = 1/2
    nf = 6
    E = [1/2, 1/2]
    x = E
    Cf = 4/3
    K = Ca*(67/18-np.pi**2/6)-5/9*nf
    Q = 1
    Q_12 = 1
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb = alpha*beta_0*L
    beta_1 = (17*Ca**2)
    B=-3/2
   
    
    g1_prime=-Cf/(np.pi*beta_0**2)*  (

        -2*(np.log(1-2*lamb))+2*(np.log(1-lamb))

    )
    
    A=-2*Cf*np.euler_gamma/(np.pi*beta_0)* (  np.log(1-lamb) -np.log(1-2*lamb)   )
    
    
    return np.exp(A)/gamma(1-beta_0*g1_prime)
        
    
    
#second order resummation (do not use)
def resum_cheap(v ,d, a, b, g,final=['q','q']):
    L = -np.log(v)
    res = 0
    alpha = 0.1175
    m_f = alpha
    Ca = 3
    TR = 1/2
    nf = 6
    E = [1/2, 1/2]
    x = E
    Cf = [4/3, 4/3]
    Q = 1
    Q_12 = 1
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb = alpha*beta_0*L
    if g != [1, 1]:
        # integral teil
        dbar = d
    else:
        dbar = d

    legs = 2
    
    
    for l in range(legs):
        res += -Cf[l]*(
            r1_cheap(L, a[l], b[l])+r2_cheap(L, a[l], b[l])*(np.log(dbar[l])-b[l]*np.log(2*E[l]/Q))
        +B_l(final[l])*T_cheap(L/(a[l]+b[l]))
        )
        + np.log(q(x[l], np.exp(-2*L/(a[l]+b[l])*m_f**2)) / q(x[l], m_f**2))

    res += np.log(Softfunction(T(L/a[0]))) + np.log( thrust_F_cheap(Ca, L) )  
    return res


def T_cheap(L,N=1):
     alpha = 0.1175
     Ca = 3
     TR = 1/2
     nf = 6
     beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
     T=0
     
     for i in range(1,N+1):
         T+=4*(4*np.pi*beta_0)**(i-1)*alpha**i*L**(i)/(i)
     
     return T
    
##second order radiator, pls ignore
def r1_cheap(L,a_l,b_l):
    L_=L.copy()
    alpha = 0.1175
    Ca = 3
    TR = 1/2
    nf = 6
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    L_*=alpha
    
    res=4*alpha/(2*np.pi)*L_/(b_l)*(1/a_l+1/(a_l+b_l))*L
    res*=0
    
    
    return res
    

 #second order r_prime, also ignore   
def r2_cheap(L,a_l,b_l):
    alpha = 0.1175
    Ca = 3
    TR = 1/2
    nf = 6
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    
    res=4*alpha*L/(b_l)*(1/a_l-1/(a_l+b_l))
    res*=0
    
    return res

#can't u read? ignore the cheap stuff, geez
def thrust_F_cheap(C,L,a=[1,1],b=[1,1]):
    Cf=4/3
    Ca = 3
    TR = 1/2
    nf = 6
    legs=len(a)
    alpha = 0.1175
    K = Ca*(67/18-np.pi**2/6)-5/9*nf
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb =alpha* beta_0*L
    beta_1 = (17*Ca**2)
    
    R=0
    for l in range(legs):
        R+=Cf*r2_cheap(L,a[l],b[l])
        
    #R=2*Cf*(   T(L/a[0])-T(L/(a[0]+b[0])) )  
        
    #2*Cf/(np.pi*beta_0**2)*(  np.log(1-2*lamb) - np.log(1-lamb)   )  
    ##
    
    #result=np.exp(-R*np.euler_gamma)/gamma(1+R)
    result=1
    return result

def resum_LO(v ,d, a, b, g,final=['q','q']):
    L=-np.log(v)
    legs=len(final)
    H11,H12=0,0
    alpha = 0.1175
    for l in range(legs):
        H11-=Colorfactor(final[l])*(4*B_l(final[l])/(a[l]+b[l]) +4/(a[l]*(a[l]+b[l]))*(np.log(d[l]))  )
        H12-=2/a[l]*Colorfactor(final[l])/(a[l]+b[l])
    return 1+H11*alpha/(2*np.pi)*L+H12*alpha/(2*np.pi)*L**2

def radiatortest2(v):
    L = -np.log(v)
    res = 0
    alpha = 0.1175
    m_f = alpha
    Ca = 3
    TR = 1/2
    nf = 6
    E = [1/2, 1/2]
    x = E
    Cf = 4/3
    K = Ca*(67/18-np.pi**2/6)-5/9*nf
    Q = 1
    Q_12 = 1
    beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
    lamb = alpha*beta_0*L
    beta_1 = (17*Ca**2)
    B=-3/2
    g1=-1/(np.pi*beta_0**2)*(
        
        (1-2*lamb)*np.log(1-2*lamb) - 2* (1-lamb)*np.log(1-lamb)
        )
    g2=-1*K/(2*np.pi**2*beta_0**2)*(     
        2*np.log(1-lamb) -  np.log(1-2*lamb)        
        )  + beta_1/(np.pi*beta_0**3)* ( 
            2*np.log(1-lamb)-np.log(1-2*lamb)+np.log(1-lamb)**2 - 1/2* np.log(1-2*lamb)**2
            
            
            )
            
    return -1/2*(1/alpha*g1+g2)


# =============================================================================
# N=2000
# t=np.linspace(0,1,N)
# resum=resum_LO(t,[1,1],[1,1],[1,1],[1,1])
# # #plt.plot(t,resum)
#   
# plt.semilogy(t[1:],N*(resum[1:]-resum[:N-1]))
# plt.semilogy(t,Thrust_analytic(1-t),label="analytic")
# plt.legend()
# plt.ylim(0.0003,10000)
# plt.show()
# =============================================================================

