#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 12:03:33 2024

@author: amber.klimke
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import fsolve
from toy_model import *

sigma=42254

print("Here,have a number:", Resummation(
    3/4, [1, 1], [1, 1], [1, 1], [1, 1]), "\n")


#compare F functions for resummation and thrust_resum


# =============================================================================
# 
# 
# 
# 
# 
#compare results from thrust resum and actual good resummation that i made, yes yes.
# =============================================================================
N = 2000
t = np.linspace(0.0,1, N)
resummed = np.exp(Resummation(t,[1,1],[1,1],[1,1],[1,1]))
plt.plot(t[1::],(t[1::])*2.3*10** 11*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="qq resum")

#plt.plot(t[1::],(t[1::])*10**11* 4*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="cheap resum")
#  
plt.plot(1-t,(1-t)*(Thrust_analytic(t)),label="NLO")
# 
# 
resummed=validate_Resummation(t,[1,1],[1,1],[1,1],[1,1])
plt.plot(t[1::],10**-7*(resummed[1::])/4,label=r"$\alpha$")

resummed = np.exp(Resummation(t,[1,1],[1,1],[1,1],[1,1],['g','g']))
plt.plot(t[1::],(t[1::])*2.3*10** 11*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="gg resum")

resummed = thrust_resummation(t)
#plt.plot(t[1::],(t[1::])*2.3*10** 11*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="thrust resum")
#plt.xlim(0,0.3)
plt.ylim(0,2)
plt.legend()
plt.xlabel("1-T")
plt.ylabel(r"$(1-T)\frac{\mathrm{d}\sigma}{\sigma\mathrm{d}(T)}$")
#plt.savefig("compare Thrust.pdf")
plt.show()
 
 
N = 2000
t = np.linspace(0.0,1, N)
resummed = np.exp(Resummation(t,[1,1],[1,1],[1,1],[1,1]))
plt.semilogy(t[1::],(t[1::])*2.3*10** 11*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="qq resum")

#plt.plot(t[1::],(t[1::])*10**11* 4*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="cheap resum")
#  
plt.semilogy(1-t,(1-t)*(Thrust_analytic(t)),label="NLO")
# 
# 
resummed=validate_Resummation(t,[1,1],[1,1],[1,1],[1,1])
plt.plot(t[1::],10**-7*(resummed[1::])/4,label=r"$\alpha$")

resummed = np.exp(Resummation(t,[1,1],[1,1],[1,1],[1,1],['g','g']))
plt.semilogy(t[1::],(t[1::])*2.3*10** 11*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="gg resum")
 #plt.xlim(0,0.3)
plt.ylim(0.001,2)
plt.legend()
plt.xlabel("1-T")
plt.ylabel(r"$(1-T)\frac{\mathrm{d}\sigma}{\sigma\mathrm{d}(T)}$")
#plt.savefig("Thrust_semilogy.pdf")
plt.show()



N = 2000
t = np.linspace(0.0,1, N)
 

def inv(y,fac=1):
    
    def quark_x(t,x=0,a=[1,1],b=[1*fac,1*fac],d=[1,1],g=[1,1]):
        return  np.exp(Resummation(t,d,a,b,g,['q','q']))-x
    
    r=[]
    for i in y:
        r.append(fsolve(lambda a: quark_x(a,x=i),0.2))
    return  r





M=inv(t)
print(max(M))
plt.plot(t,np.exp(Resummation(M,[1,1],[1,1],[1,1],[1,1],['g','g'])),label="Quark vs. Gluon Thrust")
#plt.plot(M,t,label="M")

M=inv(t,fac=0)
print(max(M))
plt.plot(t,np.exp(Resummation(M,[1,1],[1,1],[0,0],[1,1],['g','g'])),label="Quark vs. Gluon fc1")
resummed2 = np.exp(Resummation(t,[1,1],[1,1],[1,1],[1,1],['g','g']))
#plt.semilogy(t[1::],(resummed[1::]-resummed[:N-1:])/(resummed2[1::]-resummed2[:N-1:]),label=r"$\frac{qq}{gg}$")


plt.xlabel("Quark Efficiency")
plt.ylabel("Gluon Efficiency")
plt.legend()
#plt.savefig("efficiency.pdf")
#plt.xlim(0,2)
#plt.ylim(0.001,2)

# #plt.plot(t[1::],(t[1::])*10**11* 4*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="cheap resum")
plt.show()







N = 2000
t = np.linspace(0.0,1, N)
# 
resummed = np.exp(Resummation(t,[1,1],[1,1],[0,0],[1,1]))
plt.plot(t[1::],1/2*10**11*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r"qq NLL FC$_1$")
 
resummed = np.exp(Resummation(t,[1,1],[1,1],[-0.5,-0.5],[1,1]))
plt.plot(t[1::],1/2*10**11*t[1::]*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r"qq NLL FC$_\frac{3}{2}$")

resummed = np.exp(Resummation(t,[1,1],[1,1],[0,0],[1,1],['g','g']))
plt.plot(t[1::],1/2*10** 11*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r"gg NLL FC$_1$")
 
resummed = np.exp(Resummation(t,[1,1],[1,1],[-0.5,-0.5],[1,1],['g','g']))
plt.plot(t[1::],1/2*10** 11*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r" gg NLL FC$_\frac{3}{2}$")

plt.xlabel(r"FC$_x$")
plt.ylabel(r"$\frac{\mathrm{d}\sigma}{\sigma\mathrm{d}(FC)}$")
plt.legend()
#plt.savefig("FCx.pdf")
#plt.xlim(0,0.3)
plt.ylim(0,2)

#plt.plot(t[1::],(t[1::])*10**11* 4*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="cheap resum")
plt.show()
# 
# N = 2000
# t = np.linspace(0.0,1, N)
# 
# resummed = np.exp(Resummation(t,[1,1],[1,1],[0,0],[1,1]))
# plt.semilogy(t[1::],1/2*10**11*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r"qq NLL FC$_1$")
# 
# resummed = np.exp(Resummation(t,[1,1],[1,1],[-0.5,-0.5],[1,1]))
# plt.semilogy(t[1::],1/2*10**11*t[1::]*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r"qq NLL FC$_\frac{3}{2}$")
# 
# resummed = np.exp(Resummation(t,[1,1],[1,1],[0,0],[1,1],['g','g']))
# plt.semilogy(t[1::],1/2*10** 11*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r"gg NLL FC$_1$")
# 
# resummed = np.exp(Resummation(t,[1,1],[1,1],[-0.5,-0.5],[1,1],['g','g']))
# plt.semilogy(t[1::],1/2*10** 11*1/sigma*1/N*(resummed[1::]-resummed[:N-1:]),label=r" gg NLL FC$_\frac{3}{2}$")
# 
# 
# plt.xlabel(r"FC$_x$")
# plt.ylabel(r"$\frac{\mathrm{d}\sigma}{\sigma\mathrm{d}(FC)}$")
# plt.legend()
# plt.ylim(0.0001,2)
# #plt.savefig("FCx_semilogy.pdf")
# #plt.plot(t[1::],(t[1::])*10**11* 4*1/sigma*1/N*(resummed[1::]-resummed[0:N-1]),label="cheap resum")
# plt.show()
# =============================================================================


# =============================================================================
# Cf=4/3
# Ca = 3
# TR = 1/2
# alpha=0.1175
# nf = 6
# beta_0 = (11*Ca-4*TR*nf)/(12*np.pi)
# 
# t=np.linspace(0,1,2000)
# L=-np.log(t)
# fac1=-2*Cf*radiator1(L, 1, 1)+np.log(thrust_F(0, L))
# fac2=-2*Cf*radiatortest2(t)+np.log(thrust_F2(t))
# plt.semilogy(t,np.exp(fac1)* (1-alpha*beta_0*L/2)**(Cf*3/2*1/(np.pi*beta_0)),label="my resum",alpha=0.5)
# plt.semilogy(t,np.exp(fac2)* (1-alpha*beta_0*L/2)**(Cf*3/2*1/(np.pi*beta_0)),label="thrust resum",alpha=0.5)
# 
# plt.semilogy(t,np.abs(fac1-fac2),label="diff",alpha=0.5)
# plt.ylim(0.0000001,1)
# plt.legend()
# plt.show()
# 
# 
# fac1=np.exp(Resummation(t,[1,1],[1,1],[1,1],[1,1]))
# fac2=thrust_resummation(t)
# #plt.plot(t[1::],5*t[1::]*1/N*10**6*(fac1[1::]-fac1[0:N-1]),label="my resum",alpha=0.5)
# plt.plot(t[1::],5*t[1::]*1/N*10**6*(fac2[1::]-fac2[0:N-1]),label="thrust resum",alpha=0.5)
# plt.semilogy(t,fac1-fac2,label="diff",alpha=0.5)
# #plt.ylim(0.0000001,1)
# plt.legend()
# plt.show()
# 
# 
# =============================================================================


# t = np.linspace(1/2,1, N)
# y1=Thrust_analytic(t)
# plt.semilogy(t, y1,alpha=0.3,label="analytic")


# t = np.linspace(0,1.01, N)

# y2=resummed[1::]-resummed[0:N-1]

# min2= min(y2[np.logical_not( np.logical_or(np.isnan(y2) , np.isinf(y2)))])
# max2= max(y2[np.logical_not( np.logical_or(np.isnan(y2) , np.isinf(y2)))])
# min1= min(y1[np.logical_not( np.logical_or(np.isnan(y1) , np.isinf(y1)))])
# max1= max(y1[np.logical_not( np.logical_or(np.isnan(y1) , np.isinf(y1)))])


# print((max1-min1)/(max2-min2))

# plt.semilogy((3-t[1::])*0.33,  (max1-min1)/(max2-min2)*y2/100 , color='r',label="NLL' resummed")

# plt.xlim(0.4,1)
# plt.xlabel("Thrust T")
# plt.ylabel(r"$\frac{\mathrm{d}\sigma}{\sigma\mathrm{d}T}$")
# plt.legend()
# plt.savefig("compare NLL_semilog.png")
# plt.show()


#

